import math

f = open("input_day1.txt", "r")
sum_part_2 = 0
sum_part_1 = 0


def calculate_fuel_for_each_module(x) -> int:
    global sum_part_1
    x = math.floor(int(x) / 3) - 2
    sum_part_1 += x
    return sum_part_1


def calculate_total_fuel(x) -> int:
    global sum_part_2
    x = math.floor(int(x) / 3) - 2
    if (x <= 0):
        return sum_part_2
    sum_part_2 += x
    return calculate_total_fuel(x)

for x in f:
    calculate_fuel_for_each_module(x)
    calculate_total_fuel(x)

print(sum_part_1)
print(sum_part_2)
