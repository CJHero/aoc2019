from random import randint
from typing import List

f = open('input_day2.txt', 'r')
list = f.read().split(',')
test_list: List[int] = [int(i) for i in list]


def multiply():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val * second_val


def adding():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val + second_val


i = 0
test_list[1] = randint(0, 99)
test_list[2] = randint(0, 99)

while (test_list[0] != 19690720):
    test_list: List[int] = [int(i) for i in list]
    test_list[1] = randint(0, 99)
    test_list[2] = randint(0, 99)
    i = 0
    while i <= len(test_list):
        if test_list[i] == 1:
            adding()
            i += 4

        elif test_list[i] == 2:
            multiply()
            i += 4
        elif test_list[i] == 99:
            break
        else:
            i += 1

print(test_list)
print('Here are the noun and the verb : ' + str(test_list[1]) + str(test_list[2]))
