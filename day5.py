# opcode 1:  1,10,20,30   adds value [10] and [20] and puts it in [30]
# opcode 2: same as above but with multiplication
# opcode 3: takes a single int as inut and saves it to the position of its parameter
# opcode 4: outputs the value of its parameter

# parameter mode 0: position mode
# parameter mode 1: immediate mode the parameter it its value

from random import randint
from typing import List


def multiply():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val * second_val


def adding():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val + second_val


def input_val():
    input_value = input('Give me the input please. ')
    input_value = int(input_value)

    position_parameter = test_list[i + 1]
    test_list[position_parameter] = input_value


def output_val():
    position_parameter = test_list[i + 1]
    if test_list[position_parameter] == 0:
        print('we are OK!')
        print(test_list[position_parameter])

    else:
        print('Diagnostic code: ')
        print(test_list[position_parameter])


def parameter_intruction():
    opcode = str(test_list[i])[-2:]
    length_of_instruction = len(str(test_list[i]))

    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    position_to_add = test_list[i + 3]
    if (opcode == '01'):  # add
        test_list[position_to_add] = first_val + second_val
    else:  # multiply
        test_list[position_to_add] = first_val * second_val


f = open('input_day5.txt', 'r')
list = f.read().split(',')
test_list: List[int] = [int(i) for i in list]
i = 0

while i < len(test_list):
    current = test_list[i]

    current_string = str(current)
    if ((str(test_list[i])[-2:]) == '01' or (str(test_list[i])[-2:]) == '02'):
        parameter_intruction()
        i += 4
    elif (test_list[i] == 1):
        adding()
        i += 4
    elif (test_list[i] == 2):
        multiply()
        i += 4
    elif (test_list[i] == 3):
        input_val()
        i += 2
    elif (test_list[i] == 4):
        output_val()
        i += 2

    elif (test_list[i] == 99):
        break
    else:
        i += 1

print()
