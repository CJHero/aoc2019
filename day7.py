from random import randint
from typing import List
import itertools


def multiply():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val * second_val


def adding():
    first_val = test_list[test_list[i + 1]]
    second_val = test_list[test_list[i + 2]]
    position_to_add = test_list[i + 3]
    test_list[position_to_add] = first_val + second_val


def input_val():
    global input_or_combination
    global index_of_input
    global next_val_in_combination
    global y
    global first
    # print('Please give me input or phase setting ')

    position_parameter = test_list[i + 1]

    if (input_or_combination % 2 == 1):
        index_of_input = index_of_input % 5
        if first:
            input_value = 0
            first = False
        else:
            input_value = input_list[index_of_input]
            index_of_input += 1
        position_parameter = test_list[i + 1]
        test_list[position_parameter] = input_value
        input_or_combination += 1

    else:  # this happens when we need combination
        if (next_val_in_combination == 5):
            y += 1
            next_val_in_combination = 0
        input_value = list_of_combinations[y][next_val_in_combination]
        next_val_in_combination += 1
        input_or_combination += 1
    test_list[position_parameter] = input_value


def output_val():
    global highest_signal
    position_parameter = test_list[i + 1]
    input_list.append(test_list[position_parameter])
    if highest_signal < test_list[position_parameter]:
        highest_signal = test_list[position_parameter]
    if test_list[position_parameter] == 0:
        print('we are OK!')
        print(test_list[position_parameter])

    else:
        print('Diagnostic code: ')
        print(test_list[position_parameter])
    if (len(input_list) == 5):
        input_list.pop(0)
        input_list.pop(0)
        input_list.pop(0)
        input_list.pop(0)
        input_list.pop(0)


def parameter_intruction():
    opcode = str(test_list[i])[-2:]
    length_of_instruction = len(str(test_list[i]))

    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    position_to_add = test_list[i + 3]
    if (opcode == '01'):  # add
        test_list[position_to_add] = first_val + second_val
    else:  # multiply
        test_list[position_to_add] = first_val * second_val


def jump_if_true():
    length_of_instruction = len(str(test_list[i]))

    if (length_of_instruction == 1):
        if (test_list[test_list[i + 1]] != 0):
            return test_list[i + 2]
        else:
            return None

    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    if first_val != 0:
        return second_val
    else:
        return None


def jump_if_false():
    length_of_instruction = len(str(test_list[i]))

    if (length_of_instruction == 1):
        if (test_list[test_list[i + 1]] == 0):
            return test_list[test_list[i + 2]]
        else:
            return None
    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    if first_val == 0:
        return second_val
    else:
        return None


def less_than():
    length_of_instruction = len(str(test_list[i]))
    if (length_of_instruction == 1):
        if (test_list[test_list[i + 1]] < test_list[test_list[i + 2]]):
            test_list[test_list[i + 3]] = 1
        else:
            test_list[test_list[i + 3]] = 0
        return None

    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    third_val = test_list[i + 3]

    if first_val < second_val:
        test_list[third_val] = 1
    else:
        test_list[third_val] = 0


def equals():
    length_of_instruction = len(str(test_list[i]))

    if (length_of_instruction == 1):
        if (test_list[test_list[i + 1]] == test_list[test_list[i + 2]]):

            test_list[test_list[i + 3]] = 1
        else:
            test_list[test_list[i + 3]] = 0

        return None

    if (length_of_instruction == 2):
        parameter_instruction1 = '0'
        parameter_instruction2 = '0'
    if (length_of_instruction == 3):
        parameter_instruction1 = str(test_list[i])[0:1]
        parameter_instruction2 = '0'
    if (length_of_instruction == 4):
        parameter_instruction1 = str(test_list[i])[1:2]
        parameter_instruction2 = str(test_list[i])[0:1]

    if (parameter_instruction1 == '0'):
        first_val = test_list[test_list[i + 1]]
    else:
        first_val = test_list[i + 1]

    if (parameter_instruction2 == '0'):
        second_val = test_list[test_list[i + 2]]
    else:
        second_val = test_list[i + 2]

    third_val = test_list[i + 3]

    if first_val == second_val:
        test_list[third_val] = 1
    else:
        test_list[third_val] = 0


f = open('input_day7.txt', 'r')
list_of_combinations = list(itertools.permutations([0, 1, 2, 3, 4]))
highest_signal = 0

list = f.read().split(',')
test_list: List[int] = [int(i) for i in list]
i = 0
input_list = []
input_or_combination = 0
next_val_in_combination = 0

y = 0
first = True
for num_of_combinations in range(len(list_of_combinations)):

    first = True
    index_of_input = 0
    input_or_combination = 0

    for runs in range(5):

        f = open('input_day7.txt', 'r')
        list = f.read().split(',')
        test_list: List[int] = [int(i) for i in list]
        i = 0

        while i < len(test_list):
            current = test_list[i]

            current_string = str(current)
            if ((str(test_list[i])[-2:]) == '01' or (str(test_list[i])[-2:]) == '02'):
                parameter_intruction()
                i += 4
            elif (test_list[i] == 1):
                adding()
                i += 4
            elif (test_list[i] == 2):
                multiply()
                i += 4
            elif (test_list[i] == 3):
                input_val()
                i += 2
            elif (test_list[i] == 4):  # kanske ska detta vara här? or (str(test_list[i])[-2:]) == '04)'
                output_val()
                i += 2
            elif (str(test_list[i])[-2:]) == '05' or test_list[i] == 5:
                jumps = jump_if_true()
                if (jumps == None):
                    i += 3
                else:
                    i = jumps
            elif (str(test_list[i])[-2:]) == '06' or test_list[i] == 6:
                jumps = jump_if_false()
                if (jumps == None):
                    i += 3
                else:
                    i = jumps
            elif (str(test_list[i])[-2:]) == '07' or test_list[i] == 7:
                less_than()
                i += 4
            elif (str(test_list[i])[-2:]) == '08' or test_list[i] == 8:
                equals()
                i += 4
            elif (test_list[i] == 99):
                break
            else:
                i += 1
            print(test_list)

        print('Program is finished ')
        print(input_list)
        del list
        del test_list

print('________________________________')
print('dsa')
print(highest_signal)
