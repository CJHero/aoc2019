def move_right(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        global number_of_steps
        number_of_steps = number_of_steps + 1
        coordinate = ((x + i, y), number_of_steps)
        coordinates.append(coordinate)
        if is_intersect(coordinate):
            distance = abs(x + i) + abs(y)
            if (shortest_distance > distance):
                shortest_distance = distance


def move_left(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        global number_of_steps
        number_of_steps = number_of_steps + 1
        coordinate = ((x - i, y), number_of_steps)
        coordinates.append(coordinate)
        if is_intersect(coordinate):
            distance = abs(x - i) + abs(y)
            if (shortest_distance > distance):
                shortest_distance = distance


def is_intersect(coordinate):
    for item in first_wire:
        if item[0] == coordinate[0]:
            the_steps = coordinate[1] + item[1]
            coordinate = (coordinate[0], the_steps)
            intersections.append(coordinate)
            return True

    return False


def move_up(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        global number_of_steps
        number_of_steps = number_of_steps + 1
        coordinate = ((x, y + i), number_of_steps)
        coordinates.append(coordinate)
        if is_intersect(coordinate):
            distance = abs(x) + abs(y + i)
            if (shortest_distance > distance):
                shortest_distance = distance


def move_down(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        global number_of_steps
        number_of_steps = number_of_steps + 1
        coordinate = ((x, y - i), number_of_steps)
        if is_intersect(coordinate):
            distance = abs(x) + abs(y - i)
            if (shortest_distance > distance):
                shortest_distance = distance


shortest_distance = 100000
number_of_steps = 0
first_wire = [((0, 0), number_of_steps)]
intersections = [((0, 0), number_of_steps)]
coordinates = [((0, 0,), number_of_steps)]
x = 0
y = 0

with open('input_day3b.txt', 'r') as f:
    input = [line.strip() for line in f]

    for i in input:
        # print(i)
        instructions = i.split(',')
        for j in instructions:
            direction = j[0:1]
            steps = int(j[1:])
            if direction == 'R':
                move_right(steps)
                x = x + steps
            elif direction == 'L':
                move_left(steps)
                x = x - steps
            elif direction == 'U':
                move_up(steps)
                y = y + steps
            elif direction == 'D':
                move_down(steps)
                y = y - steps
        x = 0
        y = 0
        number_of_steps = 0
        first_wire = coordinates
        del coordinates
        coordinates = [(0, 0, number_of_steps)]

print(intersections)
print(shortest_distance)
