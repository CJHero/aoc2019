def move_right(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        coordinate = (x + i, y)
        coordinates.append(coordinate)
        if coordinate in first_wire:
            intersections.append(coordinate)
            distance = abs(x + i) + abs(y)
            if (shortest_distance > distance):
                shortest_distance = distance


def move_left(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        coordinate = (x - i, y)
        coordinates.append(coordinate)
        if coordinate in first_wire:
            intersections.append(coordinate)
            distance = abs(x - i) + abs(y)
            if (shortest_distance > distance):
                shortest_distance = distance


def move_up(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        coordinate = (x, y + i)
        coordinates.append(coordinate)
        if coordinate in first_wire:
            intersections.append(coordinate)
            distance = abs(x) + abs(y + i)
            if (shortest_distance > distance):
                shortest_distance = distance


def move_down(steps):
    for i in range(1, int(steps) + 1):
        global shortest_distance
        coordinate = (x, y - i)
        coordinates.append(coordinate)
        if coordinate in first_wire:
            intersections.append(coordinate)
            distance = abs(x) + abs(y - i)
            if (shortest_distance > distance):
                shortest_distance = distance


shortest_distance = 100000
first_wire = []
intersections = []
coordinates = []
x = 0
y = 0

with open('input_day3.txt', 'r') as f:
    input = [line.strip() for line in f]

    for i in input:
        # print(i)
        instructions = i.split(',')
        for j in instructions:
            direction = j[0:1]
            steps = int(j[1:])
            if direction == 'R':
                move_right(steps)
                x = x + steps
            elif direction == 'L':
                move_left(steps)
                x = x - steps
            elif direction == 'U':
                move_up(steps)
                y = y + steps
            elif direction == 'D':
                move_down(steps)
                y = y - steps
        if not first_wire:
            x = 0
            y = 0
            first_wire = coordinates
            del coordinates
            coordinates = [(0, 0)]


print(shortest_distance)
print(first_wire)
print(coordinates)
print(intersections)
