def recursion(first_val):
    for k in first_val:
        val = k[-3:]
        for x in lines:
            if val == x[:3]:
                if x not in orbits:
                    if x not in list_in_list:
                        list_in_list.append(x)

    if not list_in_list:
        return

    next_list = list_in_list.copy()
    orbits.append(next_list)
    list_in_list.clear()

    return recursion(next_list)


with open('input_day6.txt') as f:
    lines = [line.rstrip() for line in f]

orbits = []
list_in_list = []
next_list = []
first = True
i = 0

lines = [char.replace(')', '') for char in lines]

# Starts with COM, creates a list of that line and recursion does the magic.
the_list = [lines[372][:3], lines[372][:3]]
recursion(the_list)
sum = 0

for i, x in enumerate(orbits, start=1):
    print(i)
    print(x)

    sum += i * len(x)
print(sum)
print(orbits)
